package helper

import "math"

func PowInts(x, n int) int {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}

	y := PowInts(x, x/2)

	if n%2 == 0 {
		return y * y
	}
	return x * y * y
}

// GCD via Euclidean algorithm
func GCD(a, b int) int {
	// GCD requires one of the a or b can't be 0
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// LCM via GCD
func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}
	return result
}

func PrimalityTest(n int) bool {
	// check if 2 or 3, return false when <= 1
	if n <= 3 {
		return n > 1
	}
	// check if can divide by 2 or 3
	if (n%2) == 0 || (n%3) == 0 {
		return false
	}
	stop := int(math.Pow(float64(n), 0.5))
	for i := 5; i <= stop; i = i + 6 {
		if n%i == 0 || n%(i+2) == 0 {
			return false
		}
	}
	return true
}
