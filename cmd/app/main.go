package main

import (
	"fmt"
	"gitlab.com/ychvincent1022/study-golang/internal/app/helper"
)

func main() {

	// choose two distinct prime numbers p and q
	p := 11
	q := 29

	// run primality test first
	if helper.PrimalityTest(p) && helper.PrimalityTest(q) {
		fmt.Println("p = ", p)
		fmt.Println("q = ", q)
	} else {
		panic("primality test failed")
	}

	// compute n = pq
	n := p * q
	fmt.Println("n = ", n)

	// compute λ(n) where λ(n) = lcm(p − 1, q − 1)
	lambda_N := helper.LCM(p-1, q-1)
	fmt.Println("λ(n) = ", lambda_N)

}
